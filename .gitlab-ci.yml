include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - template: Container-Scanning.gitlab-ci.yml

.scripts: &scripts |
  docker_build () {
    mkdir -p ~/.docker/cli-plugins
    export BUILDXARCH="$( echo ${CI_RUNNER_EXECUTABLE_ARCH:-linux/amd64} | tr / - )"
    echo "BUILDXARCH: ${BUILDXARCH}"
    wget -O ~/.docker/cli-plugins/docker-buildx "https://github.com/docker/buildx/releases/download/v0.10.4/buildx-v0.10.4.${BUILDXARCH}"
    chmod a+rx ~/.docker/cli-plugins/docker-buildx
    docker run --privileged --rm tonistiigi/binfmt --install all

    docker buildx create --use --platform "${BUILD_ARCH}"
    docker buildx build \
      --build-arg "BUILD_DATE=$(date +"%Y-%m-%dT%H:%M:%SZ")" \
      --build-arg "BUILD_ARCH=${BUILD_ARCH}" \
      --build-arg "BUILD_REF=${CI_COMMIT_SHA}" \
      --cache-from "type=local,src=docker-build-cache" \
      --cache-to "type=local,dest=docker-build-cache,mode=max" \
      --push --tag "${IMAGE_BUILD}:${BUILD_ARCH}-${BUILD_TAG}" \
      --platform "${BUILD_ARCH}" \
       .
  }

  docker_pull_build(){
    docker pull --platform "linux/${BUILD_ARCH}" "${IMAGE_BUILD}:${BUILD_ARCH}-${BUILD_TAG}"
  }

  docker_prepare () {
    mkdir -p docker-build-cache || true
    if ! docker info &>/dev/null; then
      if [ "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      else
        export DOCKER_HOST='tcp://docker:2375'
      fi
    fi
    docker info
    if [ -n "$CI_MERGE_REQUEST_IID" ]; then
      export BUILD_TAG="${CI_MERGE_REQUEST_IID}"
    else
      export BUILD_TAG="${CI_COMMIT_SHA}"
    fi
  }

  docker_ci_login () {
    if [ -n "${CI_JOB_TOKEN}" ] ; then
      echo "${CI_JOB_TOKEN}" | docker login \
        --username gitlab-ci-token \
        --password-stdin \
        "${CI_REGISTRY}"
    else
      printf "\033[0;31m Gitlab registry login skipped, no password available\n"
    fi
  }

  docker_hub_login() {
    if [ -n "${DOCKER_LOGIN}" ] ; then
      echo "${DOCKER_PASSWORD}" | docker login \
        --username "${DOCKER_LOGIN}" \
        --password-stdin
    else
      printf "\033[0;31m Docker hub login skipped, no password available\n"
    fi
  }

  docker_manifest_create() {
    # arch, image, ref, tag, amend
    docker manifest create ${5:-} "$2:$4" "$2:$1-$3"
    docker manifest annotate "$2:$4" "$2:$1-$3" --os=linux --arch="${1}"
  }

  docker_manifest_create_amend() {
    docker_manifest_create "${1}" "${2}" "${3}" "${4}" "--amend"
  }

variables:
  DOCKER_TLS_CERTDIR: ""
  IMAGE_BUILD: ${CI_REGISTRY_IMAGE}/build
  DOCKER_DRIVER: overlay2
  DOCKER_BUILDKIT: 1    
  BUILD_AMD64: 1
  BUILD_ARM64: 1


stages:
  - .pre
  - preflight
  - build
  - test
  - scan
  - deploy
  - manifest
  - release
  # - monitor
  # - documentation
  - clean
  - .post
  
# Generic DIND template
.dind:
  image:
    name: docker:20.10.6-dind
    entrypoint: [""]
  tags:
    - docker
    - gce
  before_script:
    - *scripts
    - docker_prepare

  services:
    - name: docker:20.10.6-dind
      alias: docker

# Generic build template
.build:
  extends: .dind
  retry:
    max: 1
  stage: build
  before_script:
    - *scripts
    - docker_prepare
    - docker_ci_login
  script:
    - docker_build
  variables:
    CACHE_FALLBACK_KEY: "${BUILD_ARCH}-${CI_DEFAULT_BRANCH}"
  cache:
    key: "$BUILD_ARCH-$CI_COMMIT_REF_SLUG"
    paths:
      - docker-build-cache/
  environment:
    name: $BUILD_ARCH-$CI_COMMIT_REF_NAME
    auto_stop_in: 1 hour

# Build Jobs
build amd64:
  extends: .build
  rules:
    - if: '$BUILD_AMD64 && $BUILD_AMD64 != "false"'
  variables:
    BUILD_ARCH: amd64
  environment:
    on_stop: "cleanup amd64"
    name: build/$CI_COMMIT_REF_NAME-amd64


build arm64:
  extends: .build
  rules:
    - if: '$BUILD_ARM64 && $BUILD_ARM64 != "false"'
  variables:
    BUILD_ARCH: arm64
  environment:
    on_stop: "cleanup arm64"
    name: build/$CI_COMMIT_REF_NAME-arm64
  tags:
    - arm64

.cleanup-build:
  stage: .post
  image: alpine:3.17
  allow_failure: true
  when: manual
  variables:
    REG_SHA256: ade837fc5224acd8c34732bf54a94f579b47851cc6a7fd5899a98386b782e228
    REG_VERSION: 0.16.1
    GIT_STRATEGY: none
  before_script:
    - apk add --no-cache curl
    - curl --fail --show-error --location "https://github.com/genuinetools/reg/releases/download/v$REG_VERSION/reg-linux-amd64" --output /usr/local/bin/reg
    - echo "$REG_SHA256  /usr/local/bin/reg" | sha256sum -c -
    - chmod a+x /usr/local/bin/reg
    - /usr/local/bin/reg -h
  script:
    - /usr/local/bin/reg rm -d --auth-url $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD "${CI_PROJECT_PATH}/build:${BUILD_ARCH}-${BUILD_TAG}"
  environment:
    action: stop
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

cleanup amd64:
  extends: .cleanup-build
  variables:
    BUILD_ARCH: amd64
  environment:
    name: build/$CI_COMMIT_REF_NAME-amd64

cleanup arm64:
  extends: .cleanup-build
  variables:
    BUILD_ARCH: arm64
  environment:
    name: build/$CI_COMMIT_REF_NAME-arm64

# Generic test template
.test: &test
  extends: .dind
  before_script:
    - *scripts
    - docker_prepare
    - docker_ci_login
    - docker_pull_build
    - docker info
  stage: test

# Scan jobs
container_scanning amd64:
  extends: container_scanning
  stage: scan
  variables:
    BUILD_ARCH: amd64
    DOCKER_IMAGE: "${CI_REGISTRY_IMAGE}/build:${BUILD_ARCH}-${BUILD_TAG}"
  needs:
    - 'build amd64'
  rules:
    - if: $CONTAINER_SCANNING_DISABLED
      when: never
    - if: '$BUILD_AMD64 && $BUILD_AMD64 != "false" && $GITLAB_FEATURES =~ /\bcontainer_scanning\b/'

container_scanning arm64:
  extends: container_scanning
  stage: scan
  variables:
    BUILD_ARCH: arm64
    DOCKER_IMAGE: "${CI_REGISTRY_IMAGE}/build:${BUILD_ARCH}-${BUILD_TAG}"
  needs:
    - 'build arm64'
  rules:
    - if: $CONTAINER_SCANNING_DISABLED
      when: never
    - if: '$BUILD_ARM64 && $BUILD_ARM64 != "false" && $GITLAB_FEATURES =~ /\bcontainer_scanning\b/'

container_scanning:
  rules:
    - when: never
    
# deploy jobs
.deploy: &deploy
  extends: .dind
  variables:
    GIT_STRATEGY: none
  stage: deploy
  before_script:
    - *scripts
    - docker_prepare
    - docker_ci_login
    - docker_hub_login
    - docker_pull_build
  script:
    - TAG="${CI_COMMIT_TAG#v}"
    - TAG="${TAG:-${CI_COMMIT_SHA:0:7}}"
    # deploy to gitlab
    - >-
      docker tag
      "${IMAGE_BUILD}:${BUILD_ARCH}-${BUILD_TAG}"
      "${CI_REGISTRY_IMAGE}:${BUILD_ARCH}-${TAG}"
    - >-
      docker push \
        "${CI_REGISTRY_IMAGE}:${BUILD_ARCH}-${TAG}"
    # deploy to dockerhub
    - >-
      docker tag
      "${IMAGE_BUILD}:${BUILD_ARCH}-${BUILD_TAG}"
      "${DOCKER_HUB_ORG:-dummyorgname}/${CI_PROJECT_NAME}:${BUILD_ARCH}-${TAG}"
    - >-
      if [ -n "${DOCKER_LOGIN}" ] ; then
        docker push "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${BUILD_ARCH}-${TAG}" ;
      fi

deploy amd64:
  extends: .deploy
  variables:
    BUILD_ARCH: amd64
  rules:
    - if: '$BUILD_AMD64 && $BUILD_AMD64 != "false" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$BUILD_AMD64 && $BUILD_AMD64 != "false" && $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(?:-(?:beta|rc)(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?)?$/'

deploy arm64:
  extends: .deploy
  variables:
    BUILD_ARCH: arm64
  rules:
    - if: '$BUILD_ARM64 && $BUILD_ARM64 != "false" &&  $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$BUILD_ARM64 && $BUILD_ARM64 != "false" &&  $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(?:-(?:beta|rc)(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?)?$/'

# Manifest jobs
.manifest:
  extends: .dind
  variables:
    GIT_STRATEGY: none
  stage: manifest
  before_script:
    - *scripts
    - mkdir -p ~/.docker
    - echo '{"experimental":"enabled"}' > ~/.docker/config.json
    - docker_prepare
    - docker_ci_login
    - docker_hub_login
  script:
    - TAG="${TAG#v}"
    - TAG="${TAG:-${CI_COMMIT_SHA:0:7}}"
    - REF="${CI_COMMIT_TAG#v}"
    - REF="${REF:-${CI_COMMIT_SHA:0:7}}"
    # manifest to gitlab
    - >-
      docker_manifest_create "amd64" "${CI_REGISTRY_IMAGE}" "${REF}" "${TAG}";
    - >-
      [ -n "${BUILD_ARM64}" -a "${BUILD_ARM64}" != "false" ] && docker_manifest_create_amend "arm64" "${CI_REGISTRY_IMAGE}" "${REF}" "${TAG}";
    - >-
      docker manifest push
      ${CI_REGISTRY_IMAGE}:${TAG}
    # manifest to dockerhub
    - >-
      if [ -n "${DOCKER_HUB_ORG}" ] ; then
        docker_manifest_create "amd64" "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}" "${REF}" "${TAG}";
        [ -n "${BUILD_ARM64}" -a "${BUILD_ARM64}" != "false" ] && docker_manifest_create_amend "arm64" "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}" "${REF}" "${TAG}";
        if [ -n "${DOCKER_LOGIN}" ] ; then
          docker manifest push "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}:${TAG}";
        fi;
      fi

manifest version:
  extends: .manifest
  variables:
    TAG: "${CI_COMMIT_TAG}"
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(?:-(?:beta|rc)(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?)?$/'

manifest stable:
  extends: .manifest
  variables:
    TAG: latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(?:(?:(?:\+|\.)?[a-zA-Z0-9]+)*)?$/'
  after_script:
    - docker pull "${CI_REGISTRY_IMAGE}:latest"
    - |
      DESC="$(docker inspect ${CI_REGISTRY_IMAGE}:latest \
            -f '{{ index .Config.Labels "org.label-schema.description" }}')";
    - |
      if [ -n "${DOCKER_LOGIN}" -a -n "${DOCKER_HUB_ORG}" ] ; then
      docker run -w /code/ -v $(pwd):/code/ --rm \
        -e DOCKER_USER="${DOCKER_LOGIN}" -e DOCKER_PASS="${DOCKER_PASSWORD}" \
        mjrider/docker-update /app/dockerupdate.py \
        "${DOCKER_HUB_ORG}/${CI_PROJECT_NAME}" "${DESC}" README.md ;
      fi;

manifest edge:
  extends: .manifest
  variables:
    TAG: edge
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Running release script"
  rules:
    - if: $CI_COMMIT_TAG
  release:
    tag_name: $CI_COMMIT_TAG
    description: "$CI_COMMIT_TAG_MESSAGE"
